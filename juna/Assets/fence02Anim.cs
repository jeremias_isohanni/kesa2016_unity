﻿using UnityEngine;
using System.Collections;

public class fence02Anim : MonoBehaviour {
    //Animator objekti
    Animator anim;
    //Public muuttujat peliobjekteille ja skriptoille.
    public GameObject fence;
    public GameObject pole;
    public poleScript poleScript;
    //Public muuttujat
    public float animSpeed;
    //Private muuttujat
    private bool animStarted;
    private Vector3 posFence;
    private Quaternion rotFence;


    // Use this for initialization
    void Start()
    {
        //Ensimmäiseksi haetaan animator komponentti objektista.
        anim = GetComponent<Animator>();
        //Tämän jälkeen haetaan poleScript objektista.
        poleScript = pole.GetComponent<poleScript>();
        anim.enabled = false;
        anim.speed = animSpeed;
      
    }

    //FixedUpdatea kutsutaan aina kun objekti tekee fysiikkaan liittyvän muutokset. Normaalia updatea kutsutaan aina joka framella.
    void FixedUpdate()
    {
        //if-else lause, jolla varmistetaan että oikea animaatio suoritetaan. Ensimmäiseksi halutaan suorittaa animaatio kun juna saapuu
        //alueelle. Tämän jälkeen kun juna poistuu alueelta niin halutaan suorittaa toinen animaatio.
        if (poleScript.isTrainOnRange.Equals(true) && animStarted == false)
        {
            anim.runtimeAnimatorController = Resources.Load("Animations/fence02") as RuntimeAnimatorController;
            anim.enabled = true;
            animStarted = true;
        }
        else if (poleScript.isTrainOnRange.Equals(false) && animStarted == true)
        {
            anim.runtimeAnimatorController = Resources.Load("Animations/fence02animBack") as RuntimeAnimatorController;
            animStarted = false;
        }
    }
}
