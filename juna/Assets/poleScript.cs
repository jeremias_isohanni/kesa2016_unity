﻿using UnityEngine;
using System.Collections;

public class poleScript : MonoBehaviour {

    //Public materiaali, renderer ja bool muuttujat.
    public Color redMat = Color.red;
    public Color greenMat = Color.green;
    public Renderer rend;
    public bool isTrainOnRange = false;

    // Use this for initialization
    void Start () {
        //Haetaan objektin sisäinen komponentti nimeltään renderer.
        rend = GetComponent<Renderer>();
	}

    //Update is called once per frame
    void Update () {
        //if-lause, jolla vaihdetaan kyltin väriä rend muuttujaa käyttäen.
        if (isTrainOnRange)
        {
            rend.material.color = redMat;
        }
        else
            rend.material.color = greenMat;
	}
}
