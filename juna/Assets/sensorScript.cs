﻿using UnityEngine;
using System.Collections;

public class sensorScript : MonoBehaviour {

    //Public muuttujat peliobjekteille ja skripteille.
    public GameObject pole1;
    public GameObject pole2;
    public poleScript poleScript1;
    public poleScript poleScript2;

    // Use this for initialization
    void start()
    {
        poleScript1 = pole1.GetComponent<poleScript>();
        poleScript2 = pole2.GetComponent<poleScript>();
    }

    //Kun train objektissa liitettyyn sphere collideriin saapuu jokin objekti niin if-lauseella katsotaan onko kyseessä pole objekti.
    //Jos on niin juna on nyt rangella.
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Pole"))
        {
            poleScript1.isTrainOnRange = true;
            poleScript2.isTrainOnRange = true;
        }
    }

    //Kun train objektin sphere collideri poistuu alueelta niin annetaan info että train peliobjekti on poistunut alueelta.
    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Pole"))
        {
            poleScript1.isTrainOnRange = false;
            poleScript2.isTrainOnRange = false;
        }
    }
}
