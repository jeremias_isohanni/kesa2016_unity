﻿using UnityEngine;
using System.Collections;

public class trainMovement : MonoBehaviour {

    //Public muuttujat peliobjekteille
    public Transform[] waypoints;
    //Public muuttujat
    public float speed = 10.0F;
    public int look = 6;
    public float distance = 3.0F;
    //Private muuttujat
    private CharacterController character;
    private int currentWaypoint = 0;

    // Use this for initialization
    void Start() {
        character = GetComponent<CharacterController>(); 
    }

    //Update is called once per frame
    void Update()
    {
        //If-lause, jolla määritetään onko peliobjekti saapunut määränpäähänsä. Jos ei ole niin move() funktiota kutsutaan.
        if(currentWaypoint < waypoints.Length)
        {
            move();
        }
    }

    //move() funktio liikuttaa peliobjektia waypointin osoittamaan suuntaan.
    //Ensimmäiseksi haetaan waypointti, jonka suuntaan halutaan mennä. Tämän jälkeen Quaternionin avulla käännetään peliobjekti osoittamaan
    //oikeaan suuntaan. Viimeisenä asiana kutsutaan character muuttujaa, jotta liikkuminen voi alkaa.
    void move()
    {
        Vector3 target = waypoints[currentWaypoint].position;
        target.y = transform.position.y;
        Vector3 moveDirection = target - transform.position;

        if(moveDirection.magnitude < 0.5)
        {
            currentWaypoint++;
        }

        Quaternion rotation = Quaternion.LookRotation(target - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * look);

        character.Move(moveDirection.normalized * speed * Time.deltaTime);
    }
}
