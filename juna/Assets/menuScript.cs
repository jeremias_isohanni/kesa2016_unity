﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class menuScript : MonoBehaviour {
    
    //Public muuttujat, jotka eivät referoi objekteja ja komponentteja.
    public bool isPause;

    //Text komponentti.
    public Text pauseButton;

    //Button peliobjektit.
    public Button resetButton;
    public Button mainCameraButton;
    public Button trainCameraButton;
    public Button carCameraButton;
    public Button personCameraButton;

    //Camera peliobjektit
    public Camera mainCamera;
    public Camera trainCamera;
    public Camera carCamera;
    public Camera personCamera;

    //Private muuttujat.
    private float startTime = 1.0f;
    private float stopTime = 0.0f;
    private string m_Camera = "mainCamera";
    private string p_Camera = "personCamera";
    private string t_Camera = "trainCamera";
    private string c_Camera = "carCamera";

    // Use this for initialization
    void Start () {
        isPause = false;
    }

    //Funktio pysäyttää pelin. Tätä kutsutaan kun pelaaja painaa joko pause tai resume nappia riippuen onko peli pysäytetty vai ei.
    public void onClickPause()
    {
        if (!isPause)
        {
            setTime(stopTime);
            isPause = true;
            pauseButton.text = "Resume";
        }
        else
        {
            setTime(startTime);
            isPause = false;
            pauseButton.text = "Pause";
        }
    }

    //Funktio resetoi pelin tämän hetkiseen scenehen. Tätä funktiota kutsutaan kun pelaaja painaa "<<<" nappulaa.
    public void onClickReset()
    {
        setTime(startTime);
        setActiveCamera(m_Camera);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    //Tätä funktio kutsutaan kun peli halutaan lopettaa.
    public void onClickExit()
    {
        Application.Quit();
    }

    // -----------------------------------------------------------------------------------------------------------------
    //Seuraavia funktioita kutsutaan kun tiettyä nappia painetaan, tämä sitten lähettää pyynnön setActiveCamera funktiolle
    // joka aktivoi halutun kameran ja disabloi muut kamerat.
    public void mainCameraActive()
    {
        setActiveCamera(m_Camera);
    }

    public void trainCameraActive()
    {
        setActiveCamera(t_Camera);
    }

    public void personCameraActive()
    {
        setActiveCamera(p_Camera);
    }

    public void carCameraActive()
    {
        setActiveCamera(c_Camera);
    }
    // -----------------------------------------------------------------------------------------------------------------

    //Funktio säätää ajan. valuen arvolla 1.0f aika liikkuu normaalisti ja arvolla 0.0f peli on pysäytetty. Kutsutaan kun käyttäjä painaa
    //pause tai resume nappia tai peli aloitetaan alusta.
    private void setTime(float value)
    {
        Time.timeScale = value;
    }

    
    //Funktio vaihtaa aktiivisen kameran ja disabloi muut kamerat. Kutsutaan pelaaja haluaa vaihtaa kameraa.
    private void setActiveCamera(string i)
    {
        switch(i)
        {
            case "mainCamera":
                mainCamera.enabled = true; trainCamera.enabled = false; personCamera.enabled = false; carCamera.enabled = false;
                break;
            case "trainCamera":
                mainCamera.enabled = false; trainCamera.enabled = true; personCamera.enabled = false; carCamera.enabled = false;
                break;
            case "personCamera":
                mainCamera.enabled = false; trainCamera.enabled = false; personCamera.enabled = true; carCamera.enabled = false;
                break;
            case "carCamera":
                mainCamera.enabled = false; trainCamera.enabled = false; personCamera.enabled = false; carCamera.enabled = true;
                break;
            default: break;
        }
    }

}
